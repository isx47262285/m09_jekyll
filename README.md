# PANDOC, beamer como diapositivas.

punto de partida un documento markdown, con una  cabecera en formato yaml
el cual se convertira en una diapositiva pdf.

1. pondremos la orden que hemos utilizado y luego explicaremos 
los pasos a seguir 

```
$ pandoc apuntes_kerberos_pandoc.md -t beamer -o prova_pdf.pdf
```

2. instalacion necesaria la libreria de pandoc

3. necesitaremos una cabecera en el .md el cual se utilizara en la 
diapositiva. 

ejemplo de cabecera:
```
---
author:
- Roberto altamirano
- Peter Abelard
title:
- apuntes 
- kerberos
theme:
- AnnArbor
...

```

> apunte: importante que la cabecera se situe al principio del .md y 
tambien es necesario las marcas de principio y final de cabecera, es decir,
los '---'  y  los '...' para marcar el final.



## beamer, fichero externo.

punto de partida un documento .md el cual no tiene definida ninguna cabecera
en el propio documento, es decir, que necesitaremos importar la cabecera
externamente y que al fin consigamos convertir el .md en una diapositiva pdf.

1. orden utilizada, y procedemos a la explicacion.

```
pandoc apuntes_kerberos_pandoc.md -t beamer -M --metadata-file cap.txt -o prova_pdf.pdf
```

2. creacion de nuestra cabecera externa 

```
---
author:
- Roberto altamirano
- Peter Abelard
title:
- apuntes 
- kerberos
theme:
- AnnArbor
...
```
> importante: esta cabecera tiene que tener un formato yaml.




